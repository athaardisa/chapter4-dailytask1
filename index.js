/**
 * Impor HTTP Standar Library dari Node.js
 * Hal inilah yang nantinya akan kita gunakan untuk membuat HTTP Server
 */

// import atau panggil dotenv module (third party module)
require("dotenv").config();
const HOST = "localhost";
const PORT = process.env.PORT;
let http = require("http");
const fs = require("fs");


const data = require("./data.js");
const data1 = require("./data1.js");
const data2 = require("./data2.js");
const data3 = require("./data3.js");
const data4 = require("./data4.js");
const data5 = require("./data5.js");
const path = require("path");
const public = path.join(__dirname, "public");
const assets = path.join(__dirname, "public/assets/style.css");
const img = path.join(__dirname, "public/assets/gambar1.png");

function getHTML(htmlFileName) {
  const htmlFilePath = path.join(public, htmlFileName);
  return fs.readFileSync(htmlFilePath, "utf-8");
}

function onRequest(req, res) {
  switch (req.url) {
    case "/":
      res.writeHead(200);
      res.end(getHTML("index.html"));
      return;
    case "/assets/gambar1.png":
      fs.readFile(img, function (err, data) {
        if (err) throw err; // Fail if the file can't be read.
        res.writeHead(200, { "Content-Type": "image/png" });
        res.end(data); // Send the file data to the browser.
      });
      return;
    case "/assets/style.css":
      res.writeHead(200, { "Content-Type": "text/css" });
      var fileContents = fs.readFileSync(assets, { encoding: "utf8" });
      res.write(fileContents);
      res.end();
      return;
    case "/about":
      res.writeHead(200);
      res.end(getHTML("about.html"));
      return;
    case "/data":
      const allData = toJSON(data);
      res.setHeader("Content-Type", "application/json");
      res.writeHead(200);
      res.end(allData);
      return;
    case "/data1":
      const filter1 = toJSON(data1);
      res.setHeader("Content-Type", "application/json");
      res.writeHead(200);
      res.end(filter1);
      return;
    case "/data2":
      const filter2 = toJSON(data2);
      res.setHeader("Content-Type", "application/json");
      res.writeHead(200);
      res.end(filter2);
      return;
    case "/data3":
      const filter3 = toJSON(data3);
      res.setHeader("Content-Type", "application/json");
      res.writeHead(200);
      res.end(filter3);
      return;
    case "/data4":
      const filter4 = toJSON(data4);
      res.setHeader("Content-Type", "application/json");
      res.writeHead(200);
      res.end(filter4);
      return;
    case "/data5":
      const filter5 = toJSON(data5);
      res.setHeader("Content-Type", "application/json");
      res.writeHead(200);
      res.end(filter5);
      return;
    default:
      res.writeHead(404);
      res.end(getHTML("404.html"));
      return;
  }
}

function toJSON(value) {
  return JSON.stringify(value);
}

const server = http.createServer(onRequest);

// Jalankan server
server.listen(PORT, HOST, () => {
  console.log(`Server sudah berjalan, silahkan buka http://${HOST}:${PORT}`);
});
